<?php 
function printhead() {
?>
<html><head>
<?php if ($GLOBALS['cfg']['charset']) {?>
<meta http-equiv="Content-Type" content="text/html; charset=<?php =$GLOBALS['cfg']['charset']?>">
<meta name="robots" content="noindex, nofollow">
<?php }?>
<title>txtShout [txtbox.co.za]</title>
<link rel="stylesheet" href="<?php =$GLOBALS['cfg']['stylesheet']?>">
<?php 
}

function getztime() {
  // returns time adjusted to specified zone. arg 1: zone, 2: time
  $tme = time();$tz = 0; // Set defaults
  if (func_num_args() > 1) $tme = func_get_arg(1);
  if (func_num_args() > 0) $tz = func_get_arg(0);
  return $tme+($tz*3600)-date('Z');
}
?>