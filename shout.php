<?php
    /********************************
    *      txtShout v1.2            *
    *   �2004 - Thomas Love         *
    *   �2009 - desbest             *
    *   http://txtbox.co.za         *
    *   http://github.com/desbest   *
    ********************************/
// Use of this software is subject to 
// agreement with the terms of the 
// included license. See license.txt.

require('functions.inc.php');
require('config.inc.php');
require('txtdb.inc.php');

function strclean($s) {
  // cleans strings up - removes excess spaces and newlines.
  $s = trim($s);
  if (get_magic_quotes_gpc()) $s = stripslashes($s);
   
  $s = str_replace("\t",' ',$s);
  $s = str_replace("\n",' ',$s);
  $s = str_replace("\r",' ',$s);
  
  $slen = strlen($s);
  $nstr = '';
  for ($i=0;$i<$slen;$i++) {
    $c = substr($s,$i,1);
    if ($c == ' ') $spc++; else $spc = 0;
    if ($spc <= 1) $nstr .= $c;
  }
  return $nstr;
}

$err = '';
$nme = substr(strclean($_POST['ts_nm']), 0, $cfg['namelength']);
$url = substr(strclean($_POST['ts_ul']), 0, $cfg['urllength']);
$msg = substr(strclean($_POST['ts_mg']), 0, $cfg['msglength']);

if ($nme == $cfg['uname'] && $url == $cfg['pword']) {
  session_name('sbox');
  session_start();
  if ($_SESSION['lin']) {
    // logout
    $_SESSION = array();
    session_destroy();
    header('Location: ./index.php?err=5');
  }
  else {
    //login
    $_SESSION['lin'] = true;
    header('Location: ./index.php?err=4');
  }
  exit;
}

if ($msg == '') {
  header('Location: ./index.php?err=1');
  exit;
}

// Check email for validity
if (!empty($url)) {
  if (strlen($email) > LURL) {
    $url = '';
    $err = 2; // discarded
  }
  elseif (!preg_match('#^([a-z0-9\-_.]+?)@([\w\-]+\.([\w\-\.]+\.)?[\w]+[^\".,?! ])$#i', $url) 
  && !preg_match('#^([a-z0-9\-]+)\.([a-z0-9:\-.\~]+)((?:/[^ ]*)?[^\".,?! ])$#i',$url) 
  && !preg_match('#^([a-z]+?)://([a-z0-9\-]+)\.([a-z0-9:\-.\~]+)((?:/[^ ]*)?[^\".,?! ])#i',$url)) {
    $url = '';
    $err = 2; // discarded
  }
}

$db = new txtdb();
$db->open_table($cfg['sfile'], $cfg['sfilepath'], true) or die ($db->get_error());
$db->new_record(array(time(), $_SERVER['REMOTE_ADDR'], $nme, $url, $msg));
$db->prune_table($cfg['prune']);

header('Location: ./index.php?reload=1&err='.$err);

?>