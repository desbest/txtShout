<html>
<head>
<title>txtShout database optimisation</title>
<style>
body {
  font-family: Arial;
  font-size: 10pt;
}
</style>
</head>
<body>
<font size="+1">Optimise txtShout database</font>
<p>
This script will optimise your shouts database by recovering unused space. If your txtShout gets 
100+ new posts a day, you should run this script once every couple of months to keep txtShout 
running fast.
<p>
<hr>
<p>
<?php 
require('config.inc.php');
if (!is_file($cfg['sfile'])) die('Database not found. Check config.inc.php.');
$f = @fopen($cfg['sfile'],'r+b');
if ($f === false) die('Database could not be opened. Check file permissions.');
// file found. Check it's the old format.
$d = fgets($f, 20);	// first line
if (strlen($d) > 18) die('Database is already in new format.');
fgets($f, 20);		// second line

  if (!flock($this->f, LOCK_EX)) $this->err('Could not get lock on file');
  // create temporary file
  $tname = $store.'/temp_'.mt_rand(111111,999999);
  if (is_file($tname)) $this->err('Could not create temp file: file exists');
  $tmpf = @fopen($tname, 'wb');
  if ($tmpf === false) $this->err('Could not create temp file: cannot write');
  @chgrp($tname, TABLE_GROUP);
  @chmod($tname, TABLE_ACCESS);
  // initialise new table
  $this->init_table($tmpf, $this->get_count(), $this->get_nextid(), 0, $this->fields);
  // read all records and write them to the new table
  $nr = array();
  while ($r = $this->get_record(true)) {
    $tmp = '';
    foreach ($r as $field => $val) {
      $tmp .= $val.FS;
    }
    fwrite($tmpf, DEFAULT_RS.substr($tmp, 0, -1*strlen(FS)));
  }
  flock($this->f, LOCK_UN);
  fclose($this->f);
  fclose($tmpf);
  // switch tables
  if (is_file($store.'/'.$table.'.bak')) {
    if (!@unlink($store.'/'.$table.'.bak')) $this->err('Could not optimise: could not delete');
  }
  if (!@rename($store.'/'.$table, $store.'/'.$table.'.bak')) $this->err('Could not optimise: could not rename');
  if (!@rename($tname, $store.'/'.$table)) $this->err('Could not optimise: could not rename');
  $this->open_table($table, $store);

fclose($f);

// save a backup
rename($cfg['sfile'], $cfg['sfile'].'.bak');

// write new file
$f = @fopen($cfg['sfile'],'wb');
fwrite($f, str_pad($j, 10, '0', STR_PAD_LEFT)."\t");
fwrite($f, str_pad($j+1, 10, '0', STR_PAD_LEFT)."\t");
fwrite($f, str_pad(0, 10, '0', STR_PAD_LEFT)."\n");
fwrite($f, "id\ttme\tip\tnme\turl\tmsg");
fwrite($f, $n);

fclose($f);

echo 'All done! '.$j.' records were optimised.';


?>

</body>
</html>