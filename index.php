<?php 
error_reporting(E_ALL ^ E_NOTICE);
    /**************************
    *      txtShout v1.3      *
    *   �2004 - Thomas Love   *
    *   http://txtbox.co.za   *
    **************************/
// Use of this software is subject to 
// agreement with the terms of the 
// included license. See license.txt.

$timer = microtime() + time();

header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

require('txtdb.inc.php');
require('functions.inc.php');
require('config.inc.php');

if (isset($_COOKIE['sbox'])) {
  session_name('sbox');
  session_start();
}
if (!$cfg['reverse']) $onload = 'window.scrollTo(0,99999);';
$err = $_GET['err'];
if (is_numeric($err)) {
  switch ($err) {
    case 1: $onload .= 'alert(\'You need to type something!\');';break;
    case 2: $onload .= 'alert(\'Your e-mail / URL was discarded because it was invalid\');';break;
    case 4: $onload .= 'alert(\'Logged-in successfully!\');';break;
    case 5: $onload .= 'alert(\'Logged-out successfully!\');';break;

  }
}
?>

<?php printhead()?>
<script language="javascript">
function cb_formreset(btnonly) {
  if (parent.document.shoutform) {
  frm = parent.document.shoutform;
  frm.sub.disabled = false;
  if (!btnonly) {frm.ts_mg.value = '';frm.ts_mg.focus();}
  }
}
function del(id) {
  if (confirm('Are you sure you want to delete this post?')) document.location = "./index.php?del="+id;
}
</script>
<body class="mnbdy" <?php =($onload != '')?'onload="'.$onload.'"':''?>>
<?php 
$reload = $_GET['reload'];
if ($reload == 1) echo '<script language="JavaScript">cb_formreset(false);</script>';
elseif ($reload == 2) echo '<script language="JavaScript">cb_formreset(true);</script>';

$db = new txtdb();
//$db->create_table($cfg['sfile'], array('tme', 'ip', 'nme', 'url', 'msg'));
$db->open_table($cfg['sfile'], $cfg['sfilepath']) or die ($db->get_error());

if (isset($_GET['del']) && $_SESSION['lin']) {
  $id = (int)$_GET['del'];
  $db->delete_record($id);
}

// work out pagination
$pg = $_GET['pg'];
$scount = $db->get_count();
if ($cfg['mpp'] < 1) die('unexpected mpp config error');
$pgs = ceil($scount / $cfg['mpp']);
if ($pg > $pgs || $pg < 1) $pg = 1;
if (!$cfg['multipage']) {
  $pg = 1;
  $pgs = 1;
}

// A cheap way to work this
$skip = ($scount-(($pg-1)*$cfg['mpp'])-$cfg['mpp']);
if ($skip < 0) {
  $skip2 = 0;
  $lt = $cfg['mpp'] + $skip;
}
else {
  $skip2 = $skip;
  $lt = $cfg['mpp'];
}
$db->goto_record($skip2+1);

$i = 0;
$srow = array();
while (($srowa = $db->get_record()) && $i < $lt) {
  $srow[] = $srowa;
  $i++;
}

if ($pgs > 1 && (!$cfg['reverse'] || $pg > 1)) {?>
<table border="0" cellpadding="2" cellspacing="0" width="100%" class="hbtbl">
<tr><td class="stxt2" align="left" width="30%">&nbsp;<?php if ($pg + 1 <= $pgs) {?><a href="./?<?php =$reqquery?>&sec=main&pg=<?php =$pg+1?>">&laquo;&nbsp;prev</a><?php }?></td>
<td class="stxt2" align="center" width="40%"><span class="dtxt2"> &middot; <?php =$pg?> of <?php =$pgs?> &middot; </span></td>
<td class="stxt2" align="right" width="30%"><?php if ($pg - 1 > 0) {?><a href="./?<?php =$reqquery?>&sec=main&pg=<?php =$pg-1?>">next&nbsp;&raquo;</a><?php }?>&nbsp;</td>
</tr></table>
<br>
<?php }?>
<table border="0" cellpadding="1" cellspacing="0" width="100%">
<?php 
if (count($srow) > 0) {
  if ($cfg['reverse']) $srow = array_reverse($srow);
  
  // Create arrays of smilies and their corresponding images
  if ($cfg['smilies']) {
    require('smilies.inc.php');
    if (count($sm) > 0) {
      foreach ($sm as $code => $file) {
        $smilies_a[] = ' '.$code;
        $smilies[] = ' <img src="'.$cfg['smilies'].'/'.$file.'" border="0" width="15" height="15" alt="'.$code.'">';
      }
    }
  }

  // swear
  if ($cfg['filter']) {
    $swords = array('fuck','shit','bitch','cunt','pussy','cock','nigger');
    $swordsmsk = array();
    for ($i=0;$i<count($swords);$i++) {
      $msk = '';
      for ($j=0;$j<strlen($swords[$i]);$j++) {
        $swordsmsk[$i] .= '*';
      }
    }
  }

  for($i=0;$i<count($srow);$i++) {
    if (!$col) $col = 2;
    else $col = '';
    $str = $srow[$i]['msg'];
    $cname = $srow[$i]['nme'];
    
    $str = str_replace('&','&amp;',$str);
    $str = str_replace('<','&lt;',$str); 
    $str = str_replace('>','&gt;',$str);
    
    $cname = str_replace('&','&amp;',$cname);
    $cname = str_replace('<','&lt;',$cname);
    $cname = str_replace('>','&gt;',$cname);
    
    if ($cfg['linebreak'] > 0) {
      $cname = wordwrap($cname,$cfg['linebreak'],'<br>',1);
    }

    // swear filter
    if ($cfg['filter']) {
      for ($k=0;$k<count($swords);$k++) {
        $str = eregi_replace($swords[$k],$swordsmsk[$k],$str);
        $cname = eregi_replace($swords[$k],$swordsmsk[$k],$cname);
      }
    }
        
    // autolinking
    if ($cfg['autolink']) {
      $str = preg_replace("#([ ]|^|])([a-z]+?)://([a-z0-9@:\-]+)\.([a-z0-9@:\-.\~]+)((?:/[^ ]*)?[^][\".,?!;: ])#i", "\\1<a href=\"\\2://\\3.\\4\\5\" target=\"_blank\">\\2://\\3.\\4\\5</a>", $str);
      $str = preg_replace("#([ ]|^|])www\.([a-z0-9\-]+)\.([a-z0-9:\-.\~]+)((?:/[^ ]*)?[^][\".,?!;: ])#i", "\\1<a href=\"http://www.\\2.\\3\\4\" target=\"_blank\">www.\\2.\\3\\4</a>", $str);
      $str = preg_replace("#([ ]|^|])([a-z0-9\-_.]+?)@([\w\-]+\.([\w\-\.]+\.)?[\w]+[^][\".,?!;: ])#i", "\\1<a href=\"mailto:\\2@\\3\">\\2@\\3</a>", $str);
    }
    
    // Parse smilie codes
    if ((strstr($str,':') !== false || strstr($str,';') !== false) && $cfg['smilies']) {
      $str = str_replace($smilies_a,$smilies,' '.$str.' ');
      $str = trim($str);
    }
    
    // Line breaker. Ignores stuff in HTML tags, but not HTML entities
    if ($cfg['linebreak'] > 0) {
      $strlng = strlen($str);
      $nospc = 0;$ishtml = false;
      for($j=0;$j<$strlng;$j++) {
        if (substr($str,$j,1) == '<') $ishtml = true;
        if (!$ishtml) {
          if ($nospc > $cfg['linebreak'] - 1) {
            $str = substr($str,0,$j).'<br>'.substr($str,$j);
            $strlng += 4;
            $j += 4;
            $nospc = 0;
          }
          if (substr($str,$j,1) != ' ') $nospc++;
          else $nospc = 0;
        }
        if (substr($str,$j,1) == '>') $ishtml = false;
      }
    }
    $email2 = '';
    if (!empty($srow[$i]['url'])) {
      // work out if it's an e-mail or URL
      $email = $srow[$i]['url'];
      if (preg_match('#([ ]|^)([a-z0-9\-_.]+?)@([\w\-]+\.([\w\-\.]+\.)?[\w]+[^\".,?! ])#i',$email))
        $email2 = '<a href="mailto:'.$email.'">';
      elseif (strpos($email,'://') > 0) $email2 = '<a href="'.$email.'" target="_blank">';
      else $email2 = '<a href="http://'.$email.'" target="_blank">';
    }
?>
<tr><td class="stxt<?php =$col?>"><div class="dtxt<?php =$col?>"><?php =date('j M y, H:i', getztime($cfg['timezone'],$srow[$i]['tme']))?></div>
<?php if ($_SESSION['lin']) {?><a href="JavaScript:del(<?php =$srow[$i]['id']?>)" title="delete">[&times;]</a>&nbsp;<?php }?>
:<?php if ($email2) echo $email2?><b title="<?php =$srow[$i]['ip']?>"><?php =($cname == '')?'anonymous':$cname?></b><?php if ($email2) echo '</a>'?>: <?php =$str?> 
</td></tr>
<?php 
  }
} 
else {?>
<tr><td align="center" class="stxt"><span class="dtxt"><i>No posts</i></span></td></tr>
<?php }?>
</table>
<?php if ($pgs > 1 && ($cfg['reverse'] || $pg > 1)) {?>
<br>
<table border="0" cellpadding="2" cellspacing="0" width="100%" class="hbtbl">
<tr><td class="stxt2" align="left" width="30%">&nbsp;<?php if ($pg + 1 <= $pgs) {?><a href="./?<?php =$reqquery?>&sec=main&pg=<?php =$pg+1?>">&laquo;&nbsp;prev</a><?php }?></td>
<td class="stxt2" align="center" width="40%"><span class="dtxt2"> &middot; <?php =$pg?> of <?php =$pgs?> &middot; </span></td>
<td class="stxt2" align="right" width="30%"><?php if ($pg - 1 > 0) {?><a href="./?<?php =$reqquery?>&sec=main&pg=<?php =$pg-1?>">next&nbsp;&raquo;</a><?php }?>&nbsp;</td>
</tr></table>
<?php }?>

<!--compiled in <?php =round(((time() + microtime()) - $GLOBALS['timer'])*1000)?> milliseconds-->
</body>
</html>