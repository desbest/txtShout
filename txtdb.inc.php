<?php
    /********************************
    *   txtDB Library  v1.0         *
    *   �2004 - Thomas Love         *
    *   �2009 - desbest             *
    *   http://txtbox.co.za         *
    *   http://github.com/desbest   *
    *********************************/

error_reporting(E_ALL ^ E_NOTICE);

// These can be changed as needed.
define('TABLE_GROUP', 'wwwadmin');	// for tables created by script
define('TABLE_ACCESS', 0666);		// for tables created by script
define('OPTIMISE_AT', 1000);		// 0 = off.

// Do not change these without very good reason!
// Changing them will likely corrupt any previously-created database.
define('FS', "\t");
define('DEFAULT_RS', "\r\n");	// newline only
define('MAX_SET_LENGTH', 64);
define('MAX_FIELDS_LENGTH', 1024);
define('MAX_RECORD_LENGTH', 8178);
define('DELMRK', '*');		// non-numeric! one char only
define('ID_LENGTH', 10);
define('ID_FIELD', 'id');	// first field has to be this
define('SLASH_CHARS', "\r\n\t\\");

class TxtDB {

var $rs;
var $fs;
var $table;
var $err;
var $f;
var $fields;
var $tblopen;

// Constructor.
function txtdb () {
  return true;
}

// PRIVATE Sets the error message and if it was fatal, echos it and ends execution of script
//         Any error that prevents normal use of the database or suggests corruption of the database 
//         is fatal. Non-fatal errors can be accessed with the get_error() method. 
function err ($msg = 'No table opened', $fatal = true) {
  $this->err = $msg;
  if ($fatal) {echo '<b>TxtDB Error:</b> '.$msg; die();}
}
  
// PUBLIC Returns last error message if it wasn't fatal
function get_error () {
  return $this->err;
} 

// PUBLIC Open table if it exists
function open_table ($tbl, $store = '.', $optimise = false) {
  ignore_user_abort(true);
  $this->err = '';
  $this->table = $store.'/'.basename($tbl);
  if (!is_file($this->table)) $this->err('Table not found');
  // open and lock table
  $this->f = @fopen($this->table, 'r+b');
  if ($this->f === false) $this->err('File open error - permissions?');
  if (!flock($this->f, LOCK_EX)) $this->err('Could not get lock on file');
  // read header, determine newline code
  $tmpset = fgets($this->f, MAX_SET_LENGTH);
  $rs = substr($tmpset, -2, 3);
  if ($rs == "\r\n") $this->rs = "\r\n";
  elseif (substr($rs, -1, 1) == "\n") $this->rs = "\n";
  else $this->err('Corrupt header data: Invalid RS');
  $tmpset = substr($tmpset, 0, -1*strlen($this->rs));
  // do some paranoid validity checks. we don't actually store any state info
  $tmpset = explode(FS, $tmpset);
  if (count($tmpset) != 3) $this->err('Corrupt header data: Invalid count data');
  if (!is_numeric($tmpset[0]) || !is_numeric($tmpset[1]) || !is_numeric($tmpset[2])) $this->err('Corrupt header data: Not numeric');
  if ($tmpset[0] < 0 || $tmpset[1] < 1 || $tmpset[2] < 0) $this->err('Corrupt header data: Bad count data');
  if (strlen($tmpset[0]) != ID_LENGTH || strlen($tmpset[1]) != ID_LENGTH || strlen($tmpset[2]) != ID_LENGTH) $this->err('Corrupt header data: Incorrect size');
  // read field definitions, unlock table
  $tmpfield = fgets($this->f, MAX_FIELDS_LENGTH);
  if (substr($tmpfield, -1*strlen($this->rs), strlen($this->rs)) == $this->rs) $tmpfield = substr($tmpfield, 0, -1*strlen($this->rs));
  flock($this->f, LOCK_UN);
  $this->fields = explode(FS, $tmpfield);
  if ($this->fields[0] != ID_FIELD) $this->err('Corrupt header data: Invalid or non-existent ID field');
  // every x record deletions, optimise the table
  if ($optimise) {
    if ($tmpset[2] >= OPTIMISE_AT && OPTIMISE_AT > 0) $this->optimise_table($tbl, $store);
  }
  return true;
}

// PUBLIC Create new table if it doesn't already exist. Resource is then closed.
function create_table ($table, $fields, $store = '.') {
  $this->err = '';
  $table = $store.'/'.basename($table);
  if (is_file($table)) $this->err('Table already exists');
  // Check directory is writable
  if (!is_writable($store)) $this->err('Store directory not writable');
  // Create file
  $tmpf = @fopen($table, 'a+b');
  @chgrp($table, TABLE_GROUP);
  @chmod($table, TABLE_ACCESS);
  if ($tmpf === false) $this->err('Could not create table');
  // initialise table with fields and count values
  $this->init_table($tmpf, 0, 1, 0, $fields);
  fclose($tmpf);
  return true;
}

// PRIVATE Write count and field headers into blank table
function init_table ($fp, $rc, $nid, $dc, $fields) {
  fwrite ($fp, str_pad($rc, ID_LENGTH, '0', STR_PAD_LEFT).FS);	// record count
  fwrite ($fp, str_pad($nid, ID_LENGTH, '0', STR_PAD_LEFT).FS);	// next autoindex
  fwrite ($fp, str_pad($dc, ID_LENGTH, '0', STR_PAD_LEFT).DEFAULT_RS);	// deletion count
  if ($fields[0] != ID_FIELD) $tfields = ID_FIELD.FS; else $tfields = '';
  for ($i=0; $i<count($fields); $i++) {
    if (!ereg("[A-Za-z0-9]", $fields[$i])) $this->err('Invalid field name: '.$fields[$i]);
    $tfields .= $fields[$i].FS;
  }
  fwrite($fp, substr($tfields, 0, -1*strlen(FS)));
}

// PUBLIC Determines if specified table already exists
function is_table ($table, $store = '.') {
  $this->err = '';
  $table = $store.'/'.basename($table);
  if (is_file($table)) return true;
  else return false;
}

// PUBLIC Prune table to given number of records. This function should only be 
//        used in certain applications!
function prune_table ($records) {
  $this->err = '';
  if (!isset($this->f)) $this->err(); // table not open error
  if (!is_numeric($records) || $records < 0) $this->err('Invalid prune level');
  $todel = $this->get_count() - $records;
  if ($todel < 1) return true; // already pruned to this level
  if (!flock($this->f, LOCK_EX)) {$this->err('Could not get lock on file', false); return false;}
  $this->goto_record(1);
  $i = 0;
  while ($i < $todel && !feof($this->f)) {
    $trid = fgets($this->f, MAX_RECORD_LENGTH);
    $npos = ftell($this->f);
    $rid = substr($trid, 0, strpos($trid, FS));
    if (substr($rid, 0, 1) != DELMRK) {
      fseek($this->f, -1*strlen($trid), SEEK_CUR);
      fwrite($this->f, DELMRK);
      fseek($this->f, strlen($trid)-1, SEEK_CUR);
      $i++;
    }
  }
  $this->update_counts(-1*$i);
  flock($this->f, LOCK_UN);
}

// PUBLIC Insert new record into current table.
function new_record ($data) {
  $this->err = '';
  if (!isset($this->f)) $this->err(); // table not open error
  // +1 because of the autoid field
  if (count($data)+1 != count($this->fields)) {$this->err('Value count does not match field count', false); return false;}
  // lock the file and prepare to write
  if (!flock($this->f, LOCK_EX)) {$this->err('Could not get lock on file', false); return false;}
  $oldpos = ftell($this->f);
  // if last thing written was not a record separator, write one
  fseek($this->f, -1*strlen($this->rs), SEEK_END);
  $tmp = fread($this->f, strlen($this->rs)+1);
  if ($tmp != $this->rs) fwrite($this->f, $this->rs);
  // add id. we have to get it now, not rely on a stored value from earlier.
  $ndata = $this->get_nextid();
  // add each field
  for ($i=0; $i<count($data); $i++) {
    $ndata .= FS.addcslashes($data[$i], SLASH_CHARS);
  }
  // write if not too long
  if (strlen($ndata) > MAX_RECORD_LENGTH) {$this->err('Data too long', false); return false;}
  fwrite($this->f, $ndata);
  $this->update_counts(1);
  fseek($this->f, $oldpos);
  flock($this->f, LOCK_UN);
  return true;
}

// PUBLIC Delete record based on its ID
function delete_record ($id) {
  $this->err = '';
  if (!isset($this->f)) $this->err(); // table not open error
  if (!flock($this->f, LOCK_EX)) {$this->err('Could not get lock on file', false); return false;}
  $oldpos = ftell($this->f);
  // the find method also ensures we don't double delete
  if (!$this->find_record($id)) return false;	// the error message is generated by find_record()
  fwrite($this->f, DELMRK);
  $this->update_counts(-1);
  fseek($this->f, $oldpos);
  flock($this->f, LOCK_UN);
  return true;
}

function get_record ($raw = false) {
  $this->err = '';
  if (!isset($this->f)) $this->err(); // table not open error
  $got = false;
  $r = DELMRK;
  // move to next valid record
  while ((substr($r, 0, 1) == DELMRK || trim($r) == '') && !feof($this->f)) {
    $r = fgets($this->f, MAX_RECORD_LENGTH);
    $got = true;
  }
  if (!$got || $r == '' || substr($r, 0, 1) == DELMRK) return false;
  // remove trailing newline if it's there
  if (substr($r, -1*strlen($this->rs), strlen($this->rs)) == $this->rs) $r = substr($r, 0, -1*strlen($this->rs));
  // return record if wanted in raw form
  if ($raw) return $r;
  $r = explode(FS, $r);
  if (count($r) != count($this->fields)) $this->err('Record corruption: Incorrect field count'. $r[0]);
  $i = 0;
  // strip slashes
  foreach ($this->fields as $field) {
    $v[$field] = stripcslashes($r[$i++]);
  }
  if (!is_numeric($v[ID_FIELD])) $this->err('Record corruption: Invalid ID');
  return $v;
}

// PUBLIC Get current record count
function get_count () {
  $this->err = '';
  if (!isset($this->f)) $this->err(); // table not open error
  $oldpos = ftell($this->f);
  rewind($this->f);
  $tmpset = fgets($this->f, ID_LENGTH+1); // don't need to remove any trailing stuff.
  fseek($this->f, $oldpos);
  return (int)$tmpset;
}

// PRIVATE Get current nextid value
function get_nextid () {
  $oldpos = ftell($this->f);
  rewind($this->f);
  fseek($this->f, ID_LENGTH+strlen(FS), SEEK_CUR); // seek past count value.
  $tmpset = fgets($this->f, ID_LENGTH+1); // don't need to remove any trailing stuff.
  fseek($this->f, $oldpos);
  return (int)$tmpset;
}

// PUBLIC Find record by its position. If not found, false is returned. Starting at 0.
function goto_record ($num) {
  $this->err = '';
  if (!isset($this->f)) $this->err(); // table not open error
  if (!is_int($num) || $num < 1) {$this->err('Invalid offset', false); return false;}
  if ($num > $this->get_count()) {$this->err('Offset out of bounds', false); return false;}
  // rewind, then skip past headers
  rewind($this->f);
  fgets($this->f, MAX_SET_LENGTH);
  fgets($this->f, MAX_FIELDS_LENGTH);
  $i = 0;
  while ($i < $num-1 && !feof($this->f)) {
    // increment offset count, ignoring deleted records
    if (substr(fgets($this->f, MAX_RECORD_LENGTH),0,1) != DELMRK) $i++;
  }
  return true;
}


// PUBLIC Find record by its ID. If not found, false is returned.
function find_record ($id) {
  $this->err = '';
  if (!isset($this->f)) $this->err(); // table not open error
  if (!is_numeric($id) || $id < 1) {$this->err('Invalid ID specified', false); return false;}
  $this->goto_record(1);
  $rid = 0;
  while (((int)$rid != (int)$id || substr($rid,0,1) == DELMRK) && !feof($this->f)) {
    // Get id and check it
    // It'd be nicer to read the ID, then seek directly to next line. Impossible without fixed-length.
    // Otherwise this is the greatest speed bottleneck
    $trid = fgets($this->f, MAX_RECORD_LENGTH);
    $rid = substr($trid, 0, strpos($trid, FS));
  }
  if ($rid == $id) {
    fseek($this->f, -1*strlen($trid), SEEK_CUR);	// return to beginning of record
    return true;
  }
  else {
    $this->err('Could not find specified record', false); // left at end of file
    return false;
  }
}

// PRIVATE Updates count (and nextindex and delcount, where nec.) header and variable, ADDING $a.
//         It doesn't return the pointer to its old position. It also doesn't LOCK THE FILE. 
function update_counts ($a) {
  $a = (int)$a;
  if ($a == 0) return true;
  rewind($this->f);
  // get current counts
  $tmpset = fgets($this->f, MAX_SET_LENGTH);
  $tmpset = substr($tmpset, 0, -1*strlen($this->rs));
  $tmpset = explode(FS, $tmpset);
  rewind($this->f);
  // update count
  fwrite($this->f, str_pad((int)$tmpset[0] + $a, ID_LENGTH, '0', STR_PAD_LEFT).FS);
  if ($a > 0) {
    // adding to nextid
    fwrite($this->f, str_pad((int)$tmpset[1] + $a, ID_LENGTH, '0', STR_PAD_LEFT).FS);
  }
  elseif ($a < 0) {
    // adding to delete count
    fseek($this->f, ID_LENGTH+strlen(FS), SEEK_CUR); // seek past nextid value.
    fwrite($this->f, str_pad((int)$tmpset[2] - $a, ID_LENGTH, '0', STR_PAD_LEFT));
  }
  return true;
}  

// PUBLIC Rebuild table to reclaim space and speed used by deleted records.
//         Creates a new file, so permissions will be reset!

function optimise_table ($table, $store) {
  if (!isset($this->f)) $this->err(); // table not open error
  if (!flock($this->f, LOCK_EX)) $this->err('Could not get lock on file');

  $data = '';

  while ($r = $this->get_record(true)) {
    $data .= DEFAULT_RS.$r;
  }
  $rcount = $this->get_count();
  $nextid = $this->get_nextid();
  $fields = $this->fields;
  flock($this->f, LOCK_UN);
  fclose($this->f);
  
  // cannot make backup. any data stored at this point will be lost.
  
  // open and truncate original table (write to)
  $newf = @fopen($store.'/'.$table, 'wb');
  if ($newf === false) $this->err('Optimisation failed: could not open new DB');
  if (!flock($newf, LOCK_EX)) $this->err('Could not get lock on new file');
  
  // initialise new table and fill it (for init_table(), we don't need an open table!)
  $this->init_table($newf, $rcount, $nextid, 0, $fields);
  fwrite($newf, $data);
  
  // finish
  flock($newf, LOCK_UN);
  fclose($newf);
  $this->open_table($table, $store, false) or die ('no!');
}

}
?>