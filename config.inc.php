<?php
// txtShout Config File

$cfg = array();

// Username and password for admin (so you can delete posts). 
// To log in, uname goes in name field, pword in email/url field. 
// Hit Go _without_ typing anything in the message field.
// To log out, do the same thing again.

	$cfg['uname'] = 'uname';
	$cfg['pword'] = 'pword';


// The path to the backend file, NOT including a trailing slash or 
// filename. e.g. '/path/to/mysite/store' or './store'.

	$cfg['sfilepath'] = '.';

// The NAME of the file in which to store posts. This file must have write
// permission. Give write permission with CHMOD 646. Do not include path 
// information here.

	$cfg['sfile'] = 'shouts.txt';


// Timezone to adjust post times for. Value is decimal. Here are some common ones:
//   EET: 2  (Finland, Eastern Europe)         CET: 1  (Western Europe, Sweden)
//   GMT: 0  (United Kingdom, Portugal)        EST: -5 (NY, DC, Toronto, Montreal)
//   CST: -6 (Chicago, Houston, Winnipeg)      MST: -7 (Denver, Calgary, Edmonton) 
//   PST: -8 (LA, San Fransisco, Vancouver)
// You can use decimals for half-hours (e.g. +4:30 is 4.5)

	$cfg['timezone'] = 0;


// Character set to display posts in. Mutlibyte charsets like 
// Japanese do not work correctly with this version of txtShout.

	$cfg['charset'] = 'iso-8859-1';


// Stylesheet to use for message iframe. 

	$cfg['stylesheet'] = 'shoutstyle.css';


// The maximum lengths of the three inputs. If you change msglength, 
// be sure to change "maxMsgLength = 200;" accordingly - it's near the 
// top of the code snippet that you paste into your pages. The 
// "maxlength" input textbox attributes should also be changed. 
// All three values below must not add up to > 8000. 

	$cfg['namelength'] = 25;
	$cfg['urllength'] = 80;
	$cfg['msglength'] = 300;
	
// The maximum number of posts to store. Above this number, old posts will be deleted as new ones 
// are made. 
	$cfg['prune'] = 500;


// Forced linebreaking limit. Make this limit higher if your 
// message frame is wider, or smaller if it is narrower. If 
// not set properly, long words can mess up the layout.

	$cfg['linebreak'] = 16;


// Profanity filter. true = on, false = off. The words 
// themselves can be edited in index.php.

	$cfg['filter'] = true;


// Auto-hyperlinking for URLs / email addresses. 
// true = on, false = off

	$cfg['autolink'] = true;


// Reverse message order. The view will jump to the bottom 
// if the order is oldest->newest. 
// true = newest-to-oldest, false = oldest-to-newest.

	$cfg['reverse'] = true;


// Whether or not to allow browsing of older posts, using next / 
// prev links. true = on, false = off.

	$cfg['multipage'] = true;


// Number of posts per page. 

	$cfg['mpp'] = 20;


// Path to smilies. Check smilies.inc.php for smilie code-file 
// assignments.

	$cfg['smilies'] = 'smilies';
?>