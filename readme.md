# txtShout

Created by [txtbox](https://archive.is/lRKVm) who now runs [cbox](http://cbox.ws)
Maintained by [desbest](http://desbest.com)


txtShout is a PHP tagboard / shoutbox system that uses 
a plain text file as its database. Please read this 
file carefully for installation instructions and help.

Please read the included license.txt before installation.

If you have any questions or bugs to report, please 
send them to mail [at] txtbox.co.za


## [ INSTALLATION ]

 * Extract the archived files to their own folder.

 * Open config.inc.php in any plain-text editor.
   - Change the uname and pword variables to whatever
     you want to use to log in. 
   - Change the timezone variable to match your timezone.
   - Save the file.

 * Upload the files to your website. Keep them together in 
   a directory called 'txtshout'. If you have a previous 
   version of txtShout installed, do not overwrite your
   existing shouts.txt file!

 * Use your FTP client to CHMOD shouts.txt to 646. 

 * If your shouts.txt file was created by a version 
   of txtShout earlier than v1.3, open convert.php 
   (e.g. http://mysite.com/txtshout/convert.php) after 
   uploading to convert your old shouts database to the 
   new format. It's recommended you back-up the file 
   before converting.

 * Look in example.html for the HTML needed to put 
   txtShout in your pages, and for customisation info.


Note: avoid hand-editing the shouts.txt file. 

If you cannot display the 'txtbox' link, please 
place a link to http://www.designedge.uk.to somewhere else on your site - 
your referals help keep http://www.designedge.uk.to alive!


## [ CHANGELOG ]

 * v1.3 [04-07-24]
   - Integrated new txtDB library, which offers much 
     greater power and reliability.
   - Added auto-prune and auto-optimise (as offered by txtDB).
   - Added length validation for the message input 
     textarea.
   - Wrote new example.html.

 * v1.2 [04-05-09]
   - Fixed a few small bugs

 * v1.1 [04-04-14]
   - Record format made tab-delimited, which is basically much
     better.
   - Prev / next buttons are placed more logically.

 * v1.0 [04-04-10]
   - First release.


## [ TODO ]

 * Auto-optimisation for shouts file