<html>
<head>
<title>txtShout database convertion</title>
<style>
body {
  font-family: Arial;
  font-size: 10pt;
}
</style>
</head>
<body>
<font size="+1">Convert txtShout database</font>
<p>
This script is designed to convert txtShout databases created prior to version 1.3, 
to the new format. It will check your shouts database and will convert it if necessary. 
It only needs to be run once, and won't damage databases already in the new format.
<p>
<hr>
<p>
<?php 
require('config.inc.php');
if (!is_file($cfg['sfile'])) die('Database not found. Check config.inc.php.');
$f = @fopen($cfg['sfile'],'r+b');
if ($f === false) die('Database could not be opened. Check file permissions.');
// file found. Check it's the old format.
$d = fgets($f, 20);	// first line
if (strlen($d) > 18) die('Database is already in new format.');
fgets($f, 20);		// second line

// go through the records
while (!feof($f)) {
  $r[] = explode("\t", fgets($f, 4089));
}

// reconsitute file
$j = 0;
$n = '';
for ($i = 0; $i < count($r); $i++) {
  if (substr($r[$i][0], 1) != '-' && count($r[$i]) == 6) {
    // remove trailing newline, and reconstruct record
    $r[$i][5] = str_replace("\r", "", $r[$i][5]);
    $r[$i][5] = str_replace("\n", "", $r[$i][5]);
    $n .= "\n".($j+1)."\t".$r[$i][1]."\t".$r[$i][2]."\t".$r[$i][3]."\t".$r[$i][4]."\t".$r[$i][5];
    $j++;
  }
}

fclose($f);

// save a backup
// rename($cfg['sfile'], $cfg['sfile'].'.bak');

// write new file
$f = @fopen($cfg['sfile'],'wb');
fwrite($f, str_pad($j, 10, '0', STR_PAD_LEFT)."\t");
fwrite($f, str_pad($j+1, 10, '0', STR_PAD_LEFT)."\t");
fwrite($f, str_pad(0, 10, '0', STR_PAD_LEFT)."\n");
fwrite($f, "id\ttme\tip\tnme\turl\tmsg");
fwrite($f, $n);

fclose($f);

echo 'All done! '.$j.' records were converted to the new format.';


?>

</body>
</html>