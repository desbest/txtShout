<?php
// txtShout Smilie Config

$sm = array();

// To add a new smilie, first put the file where all the others 
// are (smilies/ directory by default), then add a new line to the bottom 
// of this file, following the pattern. The code goes in the square 
// brackets (and must have a colon or semicolon ( : or ; ) somewhere), 
// and the filename goes after the equals sign. Remember the apostrophes ( ' ) 
// and the semicolon at the end of each line!

$sm[':)'] = 'smile.gif';
$sm[':-)'] = 'smile.gif';
$sm[':biggrin:'] = 'biggrin.gif';
$sm[':cool:'] = 'toocool.gif';
$sm[':D'] = 'grin.gif';
$sm[':-D'] = 'grin.gif';
$sm[':glad:'] = 'glad.gif';
$sm[':lol:'] = 'lol.gif';
$sm[':P'] = 'tongue.gif';
$sm[':-P'] = 'tongue.gif';
$sm[';)'] = 'wink.gif';
$sm[';-)'] = 'wink.gif';
$sm[':confused:'] = 'confused.gif';
$sm[':cyclops:'] = 'cyclops.gif';
$sm[':nuts:'] = 'nuts.gif';
$sm[':o'] = 'surprised.gif';
$sm[':-o'] = 'surprised.gif';
$sm[':quizzical:'] = 'quizzical.gif';
$sm[':roll:'] = 'rollseyes.gif';
$sm[':tired:'] = 'tired.gif';
$sm[':zonked:'] = 'zonked.gif';
$sm[':/'] = 'unsure.gif';
$sm[':-/'] = 'unsure.gif';
$sm[':('] = 'sad.gif';
$sm[':-('] = 'sad.gif';
$sm[':aggrieved:'] = 'aggrieved.gif';
$sm[':aghast:'] = 'aghast.gif';
$sm[':cry:'] = 'cry.gif';
$sm[':furious:'] = 'furious.gif';
$sm[':nervous:'] = 'nervous.gif';
$sm[':x'] = 'angry.gif';
$sm[':-x'] = 'angry.gif';
$sm[':|'] = 'frown.gif';
$sm[':-|'] = 'frown.gif';
$sm[':heart:'] = 'heart.gif';

// optional extras! - uncomment to activate
// $sm[':thefinger:'] = 'thefinger.gif';
// $sm[':badass:'] = 'badass.gif';
// $sm[':nuke:'] = 'nuke.gif';
?>